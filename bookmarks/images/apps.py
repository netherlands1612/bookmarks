from django.apps import AppConfig


class ImagesConfig(AppConfig):
    name = 'images'

    def ready(self):
        # Імпортуємо файл з описаною функцією-підпискою на сигналю
        include: images.signals
